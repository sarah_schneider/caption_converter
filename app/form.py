from flask.ext.wtf import Form, validators
from wtforms.fields import RadioField, FileField

class UploadForm(Form):
    upload = FileField('upload')

class OutputForm(Form):
    output = RadioField('submit', choices=[('sami', 'Write captions in SAMI format'),
                                   ('dfxp', 'Write captions in DFXP format'),
                                   ('srt', 'Write captions in SRT format'),
                                   ('scc', 'Write captions in SCC format'),
                                   ('webvtt', 'Write captions in WebVTT format')
                                   ])