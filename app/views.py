from flask import render_template, request, redirect, url_for, send_from_directory, send_file, flash, after_this_request
import os, shutil, time, tempfile
from form import UploadForm, OutputForm
import pycaption
from werkzeug import secure_filename
from app import app
import codecs

# Messages
UPLOAD_SUCCESS = "File{0} uploaded successfully! The converter has detected {1} format{0}: {2}. Now choose an output format."
UPLOAD_ERROR = "You must upload a file! Please try again."
FORMAT_ERROR = "Error! The converter could not detect the file format. Please check the file and reupload. If the problem persists, contact IT."
EXTENSION_ERROR = "Error! Please supply a file with one of the following extentions: {0}"
CONVERSION_ERROR = "Error! Something went wrong during the conversion. Please try again. If the problem persists, contact IT."

SUPPORTED_WRITERS = {
    'sami': pycaption.SAMIWriter,
    'dfxp': pycaption.DFXPWriter,
    'srt': pycaption.SRTWriter,
    'scc': pycaption.SCCWriter,
    'webvtt': pycaption.WebVTTWriter,
}

FILE_EXTENSIONS = {
    'sami': 'sami',
    'dfxp': 'xml',
    'srt': 'srt',
    'scc': 'scc',
    'webvtt': 'vtt',
}

# Globals
upload_format_list = []
filepath_reader_dict = {}

# Initial page for file upload
@app.route('/', methods=['GET', 'POST'])
def main(message=None):
    form1 = UploadForm(prefix="form1", csrf_enabled=False)
    # Upload the file (also checks whether POST request)
    if form1.validate_on_submit() and form1.upload.name:
        # Get a list of uploaded files
        uploaded_files = request.files.getlist(form1.upload.name)
        for f in uploaded_files:
            filename = secure_filename(f.filename)
            # Save the files
            upload_filepath = save_upload(filename, f)
            # Detect the reader
            upload_format, reader = detect_reader(upload_filepath)
            upload_format_list.append(upload_format)
            # Store the detected filepath and reader in a dictionary
            filepath_reader_dict[upload_filepath] = reader
        message = message_after_upload(upload_format_list)
        
        # Suppress the uploaded formats from the list of possible conversion formats (because it's meaningless to convert xml->xml, for example)
        allowed_conversion_values = list(set(FILE_EXTENSIONS.values())-set(upload_format_list))
        global allowed_conversion_keys
        allowed_conversion_keys = []
        for u in allowed_conversion_values:
            for k, v in FILE_EXTENSIONS.items():
                if v == u:
                    allowed_conversion_keys.append(k)

        upload_format_list[:] = []

        if "Error" in message:
            return message
        else:
            flash(message)
            return redirect(url_for('convert'))

    # This is the initial GET request
    return render_template('index.html',form1=form1)

# Second page where user converts the files
@app.route('/convert', methods=['GET', 'POST'])
def convert():
    form2 = OutputForm(prefix="form2", csrf_enabled=False)
    if form2.validate_on_submit() and form2.output.data and 'convert' in request.form:
        conversion_format = form2.output.data
        # Make tmp subdirectory
        global tmpconverted
        if os.path.exists(tmpdir):
            tmpconverted = os.path.join(tmpdir, "converted/")
            os.mkdir(tmpconverted)
        else:
            flash(UPLOAD_ERROR)
            return redirect(url_for('main'))
        # Loop through uploaded filepaths and convert them
        for path in filepath_reader_dict:
            convert_file(path, conversion_format)

        # Reset dictionary
        filepath_reader_dict.clear()

        # Zip up files
        zip_filename, zip_filepath = create_zip()
        data = open(zip_filepath, 'rb')

        # Cleanup after returning zip to the user
        @after_this_request
        def remove_file(response):
            if os.path.exists(tmpdir):
                shutil.rmtree(tmpdir)
            return response
        return send_file(data, as_attachment=True, attachment_filename=zip_filename + ".zip")

    elif 'home' in request.form:
        @after_this_request
        def remove_file(response):
            filepath_reader_dict.clear()
            if os.path.exists(tmpdir):
                shutil.rmtree(tmpdir)
            return response
        return redirect(url_for('main'))

    # This is the initial GET request
    return render_template('convert.html',form2=form2, allowed_conversion_keys=allowed_conversion_keys)


### HELPER FUNCTIONS ###

# Check file extension
def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in FILE_EXTENSIONS.values()

# Determine format of uploaded files
def detect_reader(upload_filepath=None):
    if upload_filepath:
        f = open(upload_filepath)
        reader = pycaption.detect_format(f.read())
        string = str(reader).lower().split('.')[1].split('.')[0]
        upload_format = FILE_EXTENSIONS[string]
        return upload_format, reader
    else:
        return UPLOAD_ERROR

# Save uploads in a list
def save_upload(filename, f):
    if allowed_file(filename):
        global tmpdir
        tmpdir = tempfile.mkdtemp()
        upload_filepath = os.path.join(tmpdir, filename)
        f.save(upload_filepath)
        return upload_filepath
    else:
        ext_list = ["." + f for f in FILE_EXTENSIONS.values()]
        return EXTENSION_ERROR.format(', '.join(ext_list))

# Determine which message is appropriate for number of files uploaded
def message_after_upload(upload_format_list):
    num_unique_formats = len(set(upload_format_list))
    if num_unique_formats == 1:
        return UPLOAD_SUCCESS.format('', 'the', upload_format_list[0])
    elif num_unique_formats > 1:
        return UPLOAD_SUCCESS.format('s', num_unique_formats, ', '.join(set(upload_format_list)))
    else:
        return FORMAT_ERROR

# Do the conversion
def convert_file(path, conversion_format):
    output_writer_class = SUPPORTED_WRITERS[conversion_format]
    f = open(path)
    f_to_unicode_string = u'{0}'.format(f.read())
    global reader
    reader = filepath_reader_dict[path]
    converter = pycaption.CaptionConverter()
    converter.read(f_to_unicode_string, reader())
    results = converter.write(output_writer_class())
    # If conversion succeeds, write file to subdirectory
    if results:
        base_filepath = os.path.join(tmpconverted + path.rsplit('/', 1)[1].split('.', 1)[0])
        old_extension = path.split('.', 1)[1]
        new_extension = "." + FILE_EXTENSIONS[conversion_format]
        output_filepath = base_filepath + "_converted_from_" + old_extension + new_extension
        newf = codecs.open(output_filepath, 'w', encoding='utf8')
        newf.write(results)
    else:
        return CONVERSION_ERROR

# Zip up the converted files only (ignore the uploads)
def create_zip():
    zip_filename = "converted_captions_" + time.strftime('%Y%m%d-%H%M%S')
    tmparchive = os.path.join(tmpdir, zip_filename)
    zip_filepath = shutil.make_archive(tmparchive, 'zip', tmpconverted)
    return zip_filename, zip_filepath