import os
import pycaption
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
ALLOWED_EXTENSIONS = set(['sami', 'scc', 'srt', 'vtt', 'xml'])

app = Flask(__name__, instance_relative_config=False)
app.secret_key = 'the_key'
#app.config.from_object('config')
app.config.from_pyfile('config.py')
app.config['DEBUG']
app.config['ALLOWED_EXTENSIONS'] = ALLOWED_EXTENSIONS

from app import views
